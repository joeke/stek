;; Copyright 2023 Joeke de Graaf

;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at

;;     http://www.apache.org/licenses/LICENSE-2.0

;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.


;; Stek
;; Stack-implementatie voor R7RS-Scheme gebaseerd op vectors

;; Misschien zelf vector-append implementeren om deze bibliotheek in Chez te kunnen gebruiken. Chez heeft deze procedure niet (omdat deze niet in R6RS zit).


(define-library (datastructuren stek)
  (export make-stack stack-size stack-top stack-empty? stack-peek stack-pop stack-push)
  (import (scheme base)
	  (srfi 133)) ;; voor vector-append
  (begin

    (define-record-type <stek>
      (stek contents)
      stek?
      (contents get-contents set-contents!)
      (top get-top set-top!))



    (define (make-stack . size)
      (let ((stack-obj (stek (make-vector (if (null? size) 10 (car size)) '()))))
	(set-top! stack-obj 0)
	stack-obj))


    (define (stack-size stack)
      (vector-length (get-contents stack)))


    (define (stack-top stack)
      (get-top stack))


    (define (stack-empty? stack)
      (if (null? (vector-ref (get-contents stack) 0))
	  #t
	  #f))


    (define (stack-peek stack)
      (let ((top (get-top stack)))
	(if (= top 0)
	    (error "No item on stack to peek at" stack)
	    (vector-ref (get-contents stack) (- (get-top stack) 1)))))


    (define (stack-pop stack)
      (cond ((= (get-top stack) 0) (error "No item on stack to pop" stack))
	    (else
	     (let* ((top (get-top stack))
		    (temp-contents (get-contents stack))
		    (result (vector-ref temp-contents (- top 1))))
	       (vector-set! temp-contents (- top 1) '())
	       (set-contents! stack temp-contents)
	       (set-top! stack (- top 1))
	       result))))


    (define (stack-push stack element)
      (if (= (get-top stack) (stack-size stack))
	  (stack-push (stack-grow stack) element)
	  (let ((intermediate-vector (get-contents stack))
		(top (get-top stack)))
	    (vector-set! intermediate-vector top element)
	    (set-contents! stack intermediate-vector)
	    (set-top! stack (+ top 1))
	    stack)))


    (define (stack-grow stack)
      (let ((old-stack-contents (get-contents stack)))
	(set-contents! stack (vector-append old-stack-contents (make-vector (vector-length old-stack-contents) '())))
	stack))


    ))
